package com.example.dee.weatherappi;

import java.net.URLEncoder;
import java.util.LinkedList;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Dee on 11/14/16.
 */
public class WA {
    //public JsonElement jse = null;  // conditions currently Brandon, I added these Okay??
    public JsonElement jsf = null;  // forecast 10 days
    private final String token = "8280a8f526778ebe";

    public WA(String zip) {
        try {
            String encodedURL = URLEncoder.encode(zip,"utf-8");
            URL aURL = new URL("http://api.wunderground.com/api/" + token + "/conditions/forecast10day/q/" + encodedURL + ".json");

            // Make InputStream from URL
            InputStream aIs = aURL.openStream();

            // Attach to BufferedReader
            BufferedReader aBr = new BufferedReader(new InputStreamReader(aIs));

            // Attach to JSON parser
            jsf = new JsonParser().parse(aBr);

            aIs.close();
            aBr.close();



        }
        // Manage Errors by catching all possible exceptions
        // multiple catches

        catch (java.io.UnsupportedEncodingException uee) {
            uee.printStackTrace();
        } catch (java.net.MalformedURLException mue) {
            mue.printStackTrace();
        } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public LinkedList<Node> getForecast() {

        String iconURL, dayW, date, dis, tempH, tempL, cityState, iconName;


        LinkedList<Node> FC = new LinkedList<>();

        for (int i = 0; i <= 9; i++)
        {


            cityState= jsf.getAsJsonObject().
                    get("current_observation").getAsJsonObject().
                    get("display_location").getAsJsonObject().
                    get("full").getAsString();

            iconURL = jsf.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast")
                    .getAsJsonObject().get("forecastday").getAsJsonArray().get(i).getAsJsonObject().get("icon_url").getAsString();

            iconName = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().
                    get("icon").getAsString();

            tempH = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("high").
                    getAsJsonObject().get("fahrenheit").
                    getAsString();

            tempL = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();

            dayW = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString();

            String dateM = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString();

            String dateD = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString();

            String year = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();

            dis = jsf.getAsJsonObject().get("forecast").
                    getAsJsonObject().get("simpleforecast").
                    getAsJsonObject().get("forecastday").
                    getAsJsonArray().get(i).
                    getAsJsonObject().get("conditions").getAsString();

            date = dateM + "/" + dateD + "/" + year;

            Node day = new Node(iconURL, dayW, date, dis, tempH, tempL, cityState, iconName);

            FC.add(day);
        }

        return FC;

    }

    public String getCurrentTemp() {
        return jsf.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
    }


}


