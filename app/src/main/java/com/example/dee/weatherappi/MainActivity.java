package com.example.dee.weatherappi;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView high = (TextView) findViewById(R.id.theHigh);
                high.setVisibility(View.INVISIBLE);
        TextView low = (TextView) findViewById(R.id.theLow);
        low.setVisibility(View.INVISIBLE);
        TextView dayDate = (TextView) findViewById(R.id.theDayDate);
        dayDate.setVisibility(View.INVISIBLE);
        TextView forecastConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecastConditions.setVisibility(View.INVISIBLE);

        WebView radar = (WebView) findViewById(R.id.RadarView);
        radar.setVisibility(View.INVISIBLE);

        Button theCurrent = (Button) findViewById(R.id.theCurrent);
        theCurrent.setVisibility(View.INVISIBLE);

        Button the10Day = (Button) findViewById(R.id.the10Day);
        the10Day.setVisibility(View.INVISIBLE);

        Button theRadar = (Button) findViewById(R.id.theRadar);
        theRadar.setVisibility(View.INVISIBLE);

    }

    LinkedList<Node> ll;
    int i = 1;
    String zip;
    public void getWeather(View v)

    {
        // Disable threading. We'll fix this later.
        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //StrictMode.setThreadPolicy(policy);

        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.ZipCode);
        //WA w = new WA(location.getText().toString());
        //ll = w.getForecast();
        zip = location.getText().toString();
        new GetWeatherInBackground().execute(zip);

        WebView radar = (WebView) findViewById(R.id.RadarView);
        radar.loadUrl("http://api.wunderground.com/api/ca51a08ea2deeeca/radar/satellite/q/"+ zip +".png?radius=100&width=800&height=1000");
        radar.setVisibility(View.INVISIBLE);

        Button theCurrent = (Button) findViewById(R.id.theCurrent);
        theCurrent.setVisibility(View.VISIBLE);

        Button the10Day = (Button) findViewById(R.id.the10Day);
        the10Day.setVisibility(View.VISIBLE);

        Button theRadar = (Button) findViewById(R.id.theRadar);
        theRadar.setVisibility(View.VISIBLE);
        // Set the text of the temperature field


    }
    private class GetWeatherInBackground extends AsyncTask<String, Void, WA>
    {
        @Override
        protected WA doInBackground(String... locations)
        {
            WA a = new WA(locations[0]);
            ll = a.getForecast();
            return a;
        }
        @Override
        protected void onPostExecute(WA m)
        {
            i = 1;

            TextView temp = (TextView) findViewById(R.id.theTemp);
            temp.setText("" + m.getCurrentTemp() + "\u00b0");

            TextView high = (TextView) findViewById(R.id.theHigh);
            high.setText("High: " + ll.get(i).tempH + "\u00b0");
            high.setVisibility(View.INVISIBLE);

            TextView forecasteConditions = (TextView) findViewById(R.id.theForecastConditions);
            forecasteConditions.setText(ll.get(i).discription);

            TextView cityState = (TextView) findViewById(R.id.theCityState);
            cityState.setText(ll.get(0).cityState);

            TextView cHigh = (TextView) findViewById(R.id.currentHigh);
            cHigh.setText("High:" + ll.get(0).tempH + "\u00b0");

            TextView cLow = (TextView) findViewById(R.id.currentLow);
            cLow.setText("Low:" + ll.get(0).tempL + "\u00b0");

            TextView dayDate = (TextView) findViewById(R.id.theDayDate);
            dayDate.setText(ll.get(i).day + ", " + ll.get(i).date);

            TextView conditions = (TextView) findViewById(R.id.theConditions);
            conditions.setText(ll.get(0).discription);

            TextView low = (TextView) findViewById(R.id.theLow);
            low.setText("Low:" + ll.get(0).tempL + "\u00b0");

            ImageView ImageIcon = (ImageView) findViewById(R.id.theImageIcon);
            Context context = ImageIcon.getContext();
            ImageIcon.setImageResource(context.getResources().getIdentifier("drawable/" + ll.get(0).iconName, null, context.getPackageName()));

            //WebView radar = (WebView) findViewById(R.id.theRadar);
            //radar.loadUrl("http://api.wunderground.com/api/ca51a08ea2deeeca/radar/satellite/q/95677.png?radius=100&width=400&height=400");


        }
    }


    public void getCurrent(View v)
    {
        WebView radar = (WebView) findViewById(R.id.RadarView);
        radar.setVisibility(View.INVISIBLE);

        ImageView ImageIcon = (ImageView) findViewById(R.id.theImageIcon);
        Context context = ImageIcon.getContext();
        ImageIcon.setImageResource(context.getResources().getIdentifier("drawable/" + ll.get(0).iconName, null, context.getPackageName()));
        ImageIcon.setVisibility(View.VISIBLE);

        EditText zipCode = (EditText) findViewById(R.id.ZipCode);
        zipCode.setVisibility(View.VISIBLE);

        TextView temp = (TextView) findViewById(R.id.theTemp);
        temp.setVisibility(View.VISIBLE);

        Button weather = (Button) findViewById(R.id.theGetWeather);
        weather.setVisibility(View.VISIBLE);

        Button prev = (Button) findViewById(R.id.prev);
        prev.setVisibility(View.INVISIBLE);

        Button next = (Button) findViewById(R.id.theNext);
        next.setVisibility(View.INVISIBLE);

        TextView high = (TextView) findViewById(R.id.theHigh);
        high.setVisibility(View.INVISIBLE);

        TextView low = (TextView) findViewById(R.id.theLow);
        low.setVisibility(View.INVISIBLE);

        TextView dayDate = (TextView) findViewById(R.id.theDayDate);
        dayDate.setVisibility(View.INVISIBLE);

        TextView cityState = (TextView) findViewById(R.id.theCityState);
        cityState.setVisibility(View.VISIBLE);

        TextView forecastConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecastConditions.setVisibility(View.INVISIBLE);

        TextView cHigh = (TextView) findViewById(R.id.currentHigh);
        cHigh.setVisibility(View.VISIBLE);

        TextView cLow = (TextView) findViewById(R.id.currentLow);
        cLow.setVisibility(View.VISIBLE);

        TextView conditions = (TextView) findViewById(R.id.theConditions);
        conditions.setVisibility(View.VISIBLE);
    }

    public void getForecast(View v)
    {
        i=1;

        TextView high = (TextView) findViewById(R.id.theHigh);
        high.setText("High: " + ll.get(i).tempH + "\u00b0");

        TextView low = (TextView) findViewById(R.id.theLow);
        low.setText("Low: " + ll.get(i).tempL + "\u00b0");

        TextView forecasteConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecasteConditions.setText(ll.get(i).discription);

        TextView dayDate = (TextView) findViewById(R.id.theDayDate);
        dayDate.setText(ll.get(i).day + ", " + ll.get(i).date);

        WebView radar = (WebView) findViewById(R.id.RadarView);
        radar.setVisibility(View.INVISIBLE);

        ImageView ImageIcon = (ImageView) findViewById(R.id.theImageIcon);
        Context context = ImageIcon.getContext();
        ImageIcon.setImageResource(context.getResources().getIdentifier("drawable/" + ll.get(i).iconName, null, context.getPackageName()));
        ImageIcon.setVisibility(View.VISIBLE);

        EditText zipCode = (EditText) findViewById(R.id.ZipCode);
        zipCode.setVisibility(View.INVISIBLE);

        TextView cHigh = (TextView) findViewById(R.id.currentHigh);
        cHigh.setVisibility(View.INVISIBLE);

        TextView cLow = (TextView) findViewById(R.id.currentLow);
        cLow.setVisibility(View.INVISIBLE);

        TextView temp = (TextView) findViewById(R.id.theTemp);
        temp.setVisibility(View.INVISIBLE);

        Button weather = (Button) findViewById(R.id.theGetWeather);
        weather.setVisibility(View.INVISIBLE);

        Button prev = (Button) findViewById(R.id.prev);
        prev.setVisibility(View.INVISIBLE);

        Button next = (Button) findViewById(R.id.theNext);
        next.setVisibility(View.VISIBLE);

        high.setVisibility(View.VISIBLE);

        low.setVisibility(View.VISIBLE);

        dayDate.setVisibility(View.VISIBLE);

        TextView cityState = (TextView) findViewById(R.id.theCityState);
        cityState.setVisibility(View.VISIBLE);

        TextView forecastConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecastConditions.setVisibility(View.VISIBLE);

        TextView conditions = (TextView) findViewById(R.id.theConditions);
        conditions.setVisibility(View.INVISIBLE);
    }

    public void getRadar(View v)
    {
        ImageView ImageIcon = (ImageView) findViewById(R.id.theImageIcon);
        Context context = ImageIcon.getContext();
        ImageIcon.setImageResource(context.getResources().getIdentifier("drawable/" + ll.get(i).iconName, null, context.getPackageName()));
        ImageIcon.setVisibility(View.INVISIBLE);

        EditText zipCode = (EditText) findViewById(R.id.ZipCode);
        zipCode.setVisibility(View.INVISIBLE);

        TextView cHigh = (TextView) findViewById(R.id.currentHigh);
        cHigh.setVisibility(View.INVISIBLE);

        TextView cLow = (TextView) findViewById(R.id.currentLow);
        cLow.setVisibility(View.INVISIBLE);

        TextView temp = (TextView) findViewById(R.id.theTemp);
        temp.setVisibility(View.INVISIBLE);

        Button weather = (Button) findViewById(R.id.theGetWeather);
        weather.setVisibility(View.INVISIBLE);

        Button prev = (Button) findViewById(R.id.prev);
        prev.setVisibility(View.INVISIBLE);

        Button next = (Button) findViewById(R.id.theNext);
        next.setVisibility(View.INVISIBLE);

        TextView high = (TextView) findViewById(R.id.theHigh);
        high.setVisibility(View.INVISIBLE);

        TextView dayDate = (TextView) findViewById(R.id.theDayDate);
        dayDate.setVisibility(View.INVISIBLE);

        TextView cityState = (TextView) findViewById(R.id.theCityState);
        cityState.setVisibility(View.INVISIBLE);

        TextView conditions = (TextView) findViewById(R.id.theConditions);
        conditions.setVisibility(View.INVISIBLE);

        TextView forecastConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecastConditions.setVisibility(View.INVISIBLE);

        TextView low = (TextView) findViewById(R.id.theLow);
        low.setVisibility(View.INVISIBLE);

        WebView radar = (WebView) findViewById(R.id.RadarView);
        radar.setVisibility(View.VISIBLE);

    }

    public void getNext(View v)
    {
        i++;
        ImageView ImageIcon = (ImageView) findViewById(R.id.theImageIcon);
        Context context = ImageIcon.getContext();
        ImageIcon.setImageResource(context.getResources().getIdentifier("drawable/" + ll.get(i).iconName, null, context.getPackageName()));

        TextView high = (TextView) findViewById(R.id.theHigh);
        high.setText("High: " + ll.get(i).tempH + "\u00b0");

        TextView low = (TextView) findViewById(R.id.theLow);
        low.setText("Low: " + ll.get(i).tempL + "\u00b0");

        TextView forecasteConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecasteConditions.setText(ll.get(i).discription);

        TextView dayDate = (TextView) findViewById(R.id.theDayDate);
        dayDate.setText(ll.get(i).day + ", " + ll.get(i).date);

        if(i==9)
        {
            Button next = (Button) findViewById(R.id.theNext);
            next.setVisibility(View.INVISIBLE);
        }

        Button prev = (Button) findViewById(R.id.prev);
        prev.setVisibility(View.VISIBLE);
    }

    public void getPrev(View v)
    {
        i--;
        ImageView ImageIcon = (ImageView) findViewById(R.id.theImageIcon);
        Context context = ImageIcon.getContext();
        ImageIcon.setImageResource(context.getResources().getIdentifier("drawable/" + ll.get(i).iconName, null, context.getPackageName()));

        TextView high = (TextView) findViewById(R.id.theHigh);
        high.setText("High: " + ll.get(i).tempH + "\u00b0");

        TextView low = (TextView) findViewById(R.id.theLow);
        low.setText("Low: " + ll.get(i).tempL + "\u00b0");

        TextView forecasteConditions = (TextView) findViewById(R.id.theForecastConditions);
        forecasteConditions.setText(ll.get(i).discription);

        TextView dayDate = (TextView) findViewById(R.id.theDayDate);
        dayDate.setText(ll.get(i).day + ", " + ll.get(i).date);

        if(i==1)
        {
            Button prev = (Button) findViewById(R.id.prev);
            prev.setVisibility(View.INVISIBLE);
        }

        Button next = (Button) findViewById(R.id.theNext);
        next.setVisibility(View.VISIBLE);

    }

}
